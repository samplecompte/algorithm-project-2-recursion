#Sam LeCompte
#1/14/16
#Block 2

def fib(n):
    if n == 0:
        return 0
    elif n == 1:
        return 1
    else:
        return fib(n-2) + fib(n-1)

def fibLoop(n):
    indlist = [0,1]
    num = 0
    for x in range(2, n+1):
        num = indlist[x-1] + indlist[x-2]
        indlist.append(num)

    return indlist[n]




userInput = int(input("Enter a positive integer:  "))
print("Loops: " + "The fibonacci number at point " + str(userInput) + " is " + str(fibLoop(userInput)))
print("Recusion: " + "The fibonacci number at point " + str(userInput) + " is " + str(fib(userInput)))


# Loops are more efficent than recursion because they only call one function. Recursion creates a large call
# stack that then has to track back on itself. This takes both more time and more memory. 