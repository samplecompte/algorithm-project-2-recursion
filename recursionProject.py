#Sam LeCompte
#Block 2
#1/7/16
x = 0
def sumDigits(n):
    global x
    if len(n) == 0:
        return 0
    else:
        x += int(n.pop())
        sumDigits(n)

str_x = input("Please enter a positive integer:  ")

xList = list(str_x)

sumDigits(xList)
print("The sum of the digits in " + str(str_x) +  " is " + str(x))